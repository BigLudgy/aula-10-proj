package pt.uevora;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class MyCalculatorTest {

    MyCalculator calculator;

    @Before
    public void setUp(){
        calculator = new MyCalculator();
    }

    @After
    public void down(){
        calculator = null;
    }

    //teste para a adição
    @Test
    public void testSum() throws Exception {

        Double result = calculator.execute("22+4");

        assertEquals("The sum result of 22 + 4 must be 26",  26D, (Object)result);
    }

    // teste para a subtração
    @Test
    public void testSubtraction() throws Exception {

        Double result = calculator.execute("39-5");

        assertEquals("The result of 39-5 must be 34", 34D, (Object)result);
    }

    //teste para a multiplicação
    @Test
    public void testMultiplicatino() throws Exception {

        Double result = calculator.execute("2*6");

        assertEquals("The result of 2*6 must be 12", 12D, (Object)result);
    }

    //teste para a divisão
    @Test
    public void testDivision() throws Exception {

        Double result = calculator.execute("21/3");

        assertEquals("The result of 21/3 must be 7", 7D, (Object)result);
    }

    // teste introdução de um argumento válido
    @Test(expected = IllegalArgumentException.class)
    public void testInvalidInput() throws Exception {

        calculator.execute("ok");

    }
}
